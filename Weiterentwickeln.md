Warum muss sich der GenerationD-Wettbewerb weiterentwickeln?

Mit Blick auf die Bewerbezahlen und das Sponsoring ist der
GenerationD-Wettbewerb in einer soliden Ausgangsposition.

Die Bewerberzahlen haben in den letzten Jahren stetig zugenommen.
Weiterhin gibt es Sponsoren, die unseren Wettbewerb langfristig
unterstützen und eine jährliche Ausschüttung von über 10.000€ an die
Gewinnerteams ermöglichen. Durch ideelle Förderung ist es möglich,
Workshops und Coachings für die Startups anzubieten. Diese Diversität
ist unserer Meinung nach wichtig, um den Bewerbern eine wirkliche
Unterstützung zu geben. Das Ziel des Wettbewerbs ist somit eindeutig
nicht kompetitiv, also die Vergleich der Startups untereinander, sondern
eine individuelle Förderung. Durch die Beschränktheit der Mittel ist der
Wettbewerb zwischen den Bewerbern eine Möglichkeit, diese fair zu
verteilen. Allerdings ist dieser leistungsbezogene Ansatz nicht das
einzige Bewertungskriterium: Daneben ist für die Jury auch zentral,
welche positive Wirkung mit den Wettbewerbsmitteln erreicht werden kann.
Hierbei liegt besonderer Wert darauf, Ideen in einer frühen Phase zu
unterstützen und damit deren Umsetzung zu ermöglichen. Diese Balance von
Leistungs- und Bedarfsgerechtigkeit ist die zentrale Aufgabe der
Wettbewerbsjury.

Da die Maximierung des ökologischen und sozialen Impacts zentral in der
Bewertung der Bewerber ist, soll auch der Wettbewerb als solcher unter
diesen Gesichtspunkten betrachtet werden.

Dabei lässt sich feststellen, dass in den letzten Jahren das Commitment
der Wettbewerbsteilnehmer insgesamt abgenommen hat. Insbesondere die
Anwesenheit bei den Workshops ist zurückgegangen. Dies ist insbesondere
schade, da in diesen Events die Sponsoren selbst aktiv werden und ihre
eigenen Kompetenzen an die Teilnehmer vermitteln können.

Es stellt sich die Frage, was die Gründe für diesen Wandel sind. Soweit
wir es beurteilen können ist es keineswegs die fehlende Motivation der
einzelnen Teams, sondern eher eine Veränderung des Umfelds für die
Startups. Während des Bestehens des Wettbewerbs hat es geradezu eine
Explosion an anderen Förderungsmöglichkeiten gegeben. Auf der einen
Seite sind das viele andere Wettbewerbe (zum Beispiel fuer-gruender.de
nennt 170 für das Jahr 2019), auf der anderen Seite ist das Thema auch
an den Hochschulen präsent, die Gründung und Unternehmertum in die Lehre
integrieren.

Wie wirkt sich das auf GenerationD aus? Zusammenfassend lässt sich
sagen, dass die drei Kernpunke, also Wettbewerb, finanzielle Förderung
und ideelle Förderung keineswegs ein Alleinstellungsmerkmal sind.
Insbesondere bei der finanziellen Förderung gibt es teilweise wesentlich
größere Summen als Preisgeld.

**

Nun stellt sich die Frage, auf welche Art und Weise GenerationD auch in
Zukunft eine Vorreiterrolle einnehmen kann. Im folgenden sind einige
Gedanken formuliert, grundlegend für die Veränderung des
GenerationD-Wettbewerbs sein sollen:

-   Der *soziale und ökologische Mehrwert* soll auch in Zukunft das
    zentrale Anliegen von GenerationD sein. Wir als Organisationsteam
    stellen unser Engagement unter die Agenda eines
    nachhaltigen Handelns.
-   Um dieses Ziel zu erreichen wollen wollen wir einerseits *Ideen und
    Konzepte fördern*. Wir denken, dass . Andererseits wollen wir auch
    *Individuen und Teams auf einer persönlichen Ebene fördern* und
    dabei unterstützen Wandel in der Gesellschaft umzusetzen. Konkrete
    Maßnahmen, wie dieses umgesetzt werden soll sind in den folgenden
    Kapiteln aufgeführt.
-   Wir wollen ein möglichst *diverses Profil an Teilnehmern* erreichen
    und unser Ansatz ist *interdisziplinär*. Unter den gleichen
    Gesichtspunkten soll erwähnt werden, dass insbesondere auch
    technische Innovationen Treiber des Wandels in der Gesellschaft
    sein können. Ein konkretes Beispiel hierfür ist das Gewinnerteam des
    Wettbewerbs 2018, Acrai, die einen autonomen Unkrautroboter
    entwickelt haben und damit den Einsatz von Herbiziden in der
    Landwirschaft verringern wollen.
-   Als Wettbewerb sind wir nicht an einer direkten Profitabilität
    interessiert, sondern unser Ansatz ist langfristig. Das Ziel ist es,
    *Ideen und Konzepte zu fördern, sodass sie sich selbst tragen
    können*. Das Netzwerk, dass in Zukunft mehr im Mittelpunkt stehen
    soll, soll dem selben Ziel dienen.
-   Wir wollen auch weiterhin die *Kompetenzen der Sponsoren nutzen* und
    ihnen im Programm die Möglichkeit geben, mit dem Teilnehmern in
    Kontakt zu kommen. Die unterschiedlichen Hintergründe und
    Erfahrungen der Sponsoren sind etwas, was GenerationD
    einzigartig macht. Weiterhin ist es eine *passende Ergänzung zum
    Profil des Organisationsteams*, das sich aus
    Studenten zusammensetzt.
-   Der Wettbewerb soll auch weiterhin durch ein studentisches Team
    geführt werden, es soll *keine Professionalisierung stattfinden*.
    Die Erhöhung des Impacts rein durch eine Erhöhung des
    ausgeschütteten Preisgeldes ist nicht das Ziel.
-   Die Einzigartigkeit von GenerationD soll kein Selbstzweck sein.
    Stattdessen wollen wir ein Angebot schaffen, das die *bestehenden
    Lücken schließt und die Teilnehmer in das Zentrum des Wettbewerbs
    stellt*. Wir wollen eine Plattform für gesellschaftlichen
    Wandel darstellen.

Wie soll GenerationD in Zukunft aussehen?

Bei der hier vorgeschlagenen Transformation handelt es sich um eine
tiefgreifende Erneuerung und Umgestaltung. Das Motto des Wandels könnte
man Zusammenfassen als:

 „**Weg vom Wettbewerb des Bestehenden – hin zur Begleitung des
Werdenden“**

In Zukunft soll GenerationD eine Anlaufstelle für alle werden, die durch
eine Idee einen positiven Wandel herbeiführen wollen.

Was soll GenerationD konkret für Rollen übernehmen?

-   Motivation und Zusammenhang schaffen

    -   Wir gehen davon aus, dass in vielen Leuten der Wunsch nach
        Veränderung und Eigeninitiative vorhanden ist, aber sich
        nie manifestiert. Durch GenerationD soll ein Format geschaffen
        werden, dass eine niedrige Einsteigsschwelle hat, aber dennoch
        motiviert, sich mit seinen Ideen auseinanderzusetzen
    -   Durch einen gestaffelten Aufbau soll sowohl die Idee als auch
        der Mensch oder das Team dahinter wachsen. Außerdem soll die
        Struktur einen Rahmen vorgeben, der bei der Entwicklung
        unterstützend wirkt. Auf diese Weise soll die erste Erfahrung,
        selbst Initiative zu zeigen, begleitet werden.

-   Leute verbinden und ähnlich gesonnene Menschen finden

    -   Wir vermuten, dass durch die Konstruktion von GenerationD
        motivierte Leute zusammenkommen, die sich gegenseitig
        inspirieren können.
    -   Durch die gemeinsame Arbeit am eigenen Projekt können die

-   Information bündeln

    -   Wir wollen Menschen animieren, sich zu engagieren und eigene
        Ideen umzusetzen. Um einen Startpunkt für dieses Engagement zu
        geben wollen wir Informationen zusammentragen, die bei diesem
        Schritt helfen.
    -   Unser Handeln soll transparent sein und wir verpflichten uns,
        einen Möglichst großen Teil der Unterlagen und Hilfestellungen
        öffentlich verfügbar zu machen. Die Webseite soll einen
        Ausgangspunkt darstellen, um die Informationen zu teilen.

-   Kompetenzen von etablierten Seiten nutzen

    -   

  ----------------- ------------------------------------------ -----------------------------------------------------------------------------
  ----------------- ------------------------------------------ -----------------------------------------------------------------------------

1\. Ein zentrales Argument sozialer mehrwert

2\. Wir sind nicht an der direktenProfitabilität interessiert → in the
long run, änderung des denkens der Teilnemher

3\. es ist kein ziel, den umfang der finanziellen förderung um das
mehrfache zu erhöhen

4\. Der wettbewerbs soll auch weiterhing so geführt werden, wie es
aktuell gamcht wird, es soll keine professionalisierung stattfinden

5\. Wir wollen die Kompetenzen der Sponsoren nutzen (KPMG, Bayer, SZ) →
sind auf jeden fall etwas, was uns einzigartig macht

6\. Einzigartigkeit nicht als selbstzweck, sondern wir wollen die
Teilnehmer in das Zentrum unseres wettbewerbs stellen

Was soll das Selbstbild des Wettbewerbs sein?

Ist es unsere Aufgabe, das gesellschaftliche Klima zu verändern oder
individuelles engagement zu Fördern?

Welche Themen soll GenerationD behandeln?

Sektoren: Nachhaltiges Wirtschaften

Auf welche Region soll unser Fokus gelegt werden? Sollen wir uns
Fokussieren?

Wie soll die einschränkung der Wettbewerbsteilnehmer stattfinden? Sollen
wir überhaupt eine Einschränkung vornehmen?

Wie soll sich unsere Zielgruppe verändern?

Was ist für uns als Organisationsteam wichtig?

Wir haben einen eher systemischen Ansatz →

Wie kann das mit den Teams in den Einklang gebracht werden?

Was ist die Auswirkung jedes einzelnen?

Wichting → Was ist die persönliche Einstellung des Organisationsteams
und was ist die Ausrichtung des Wettbewerbs? Wie kann beides zur deckung
gebracht werden?

→ relativ starker Wandel → wie kann man die kontinuität sicherstellen? →
ist das was wir uns vorstellen noch im Rahmen eines allgemeninen
Wandels?


